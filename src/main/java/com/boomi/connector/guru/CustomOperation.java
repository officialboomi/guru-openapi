// Copyright (c) 2022 Boomi, Inc.

package com.boomi.connector.guru;

import com.boomi.connector.openapi.OpenAPIOperation;

public class CustomOperation extends OpenAPIOperation {

    protected CustomOperation(CustomOperationConnection connection) {
        super(connection);
    }
}
